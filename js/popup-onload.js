/**
 * @file
 * Popup Onload js file.
 */

(function ($, window, Drupal) {
  'use strict';

  $(document).ready(function () {
    jQuery.ajax({url: "/popup_onload/get_popup?path=" + window.location.pathname, success: function(result) {
        if (result) {
          var popupSettings = JSON.parse(result);
          if (popupSettings) {
            setTimeout(function () {
              var $previewDialog = $('<div />').html(popupSettings.html).appendTo('body');
              Drupal.dialog($previewDialog, popupSettings).showModal();
            }, popupSettings.delay);
          }
        }
      }});
  });

})(jQuery, window, Drupal);
