<?php

namespace Drupal\popup_onload\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PopupOnloadController.
 */
class PopupOnloadController extends ControllerBase {

  /**
   * Get popup.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return response with popup settings.
   */
  public function getPopup() {
    $response = new Response();
    $popups = popup_onload_get_popups_all();

    foreach ($popups as $key => $popup) {
      if (isset($popup->path->value)) {
        if (popup_onload_check_path($popup->path->value) && popup_onload_check_display_conditions($popup)) {
          $popup_settings = popup_onload_prepare_popup($popup);
          $response->setContent(json_encode($popup_settings));
          drupal_static(POPUP_ONLOAD_IS_POPUP_ADDED, TRUE);
          popup_onload_save_time_cookie($popup);
          break;
        }
      }
      else {
        if (popup_onload_check_path('') && popup_onload_check_display_conditions($popup)) {
          $popup_settings = popup_onload_prepare_popup($popup);
          $response->setContent(json_encode($popup_settings));
          drupal_static(POPUP_ONLOAD_IS_POPUP_ADDED, TRUE);
          popup_onload_save_time_cookie($popup);
          break;
        }
      }
    }

    return $response;
  }

}
